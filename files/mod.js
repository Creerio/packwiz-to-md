/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const Resource = require('./resource');
const { isVerbose, firstCharacterUpper, getSourceUrlName } = require('../utils');

/**
 * Represents a mod/library
 */
class Mod extends Resource {
    #modDepending = [];

    addDependingMod(mod) {
        this.#modDepending.push(mod);
    }

    /**
     * Returns a markdown line of this resource
     * @returns {string}
     */
    getLibMdLine() {
        if (isVerbose())
            console.debug('Preparing to return library markdown line for ' + this.getName());
        let res = '| ' + this.getName() + ' | ';

        res += this.#modDepending.map((mod) => {
            return mod.getName();
        }).join(', ') + ' | ';

        res += this.getMdLinks() + ' |\n';

        return res;
    }
}

module.exports = Mod;