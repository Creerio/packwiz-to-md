/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const path = require('path');
const toml = require('toml');

/**
 * Used to read the pack file, mostly to find every file to parse
 */
class Pack {
    static packFile = 'pack.toml';
    #content;

    /**
     * Builds the class by parsing the file
     * @param readPath Path to the file
     */
    constructor(readPath) {
        const packPath = path.join(readPath, Pack.packFile);
        if (!fs.existsSync(packPath)) {
            console.error('No ' + Pack.packFile + ' found in ' + readPath);
            process.exit(1);
        }

        // Parse toml file first to get its content
        this.#content = fs.readFileSync(packPath).toString();
        try {
            this.#content = toml.parse(this.#content);
        }
        catch (e) {
            console.error('Failed to parse ' + packPath + ' see the error below :');
            console.error('Parsing error on line ' + e.line + ', column ' + e.column + ': ' + e.message);
            process.exit(1);
        }

        this.files = [];
        for (const elem in this.#content) {
            if (this.#content[elem]['file'])
                this.files.push(path.join(readPath, this.#content[elem]['file']));
        }
    }

    /**
     * Returns the files inside the pack.toml file
     * @returns {[]}
     */
    getFiles() {
        return this.files;
    }

    /**
     * Returns the defined packwiz name
     * @returns {string}
     */
    getName() {
        return this.#content['name'];
    }

    /**
     * Returns the packwiz author
     * @returns {string}
     */
    getAuthor() {
        return this.#content['author'] ? this.#content['author'] : '';
    }

    /**
     * Returns the packwiz version
     * @returns {string}
     */
    getVersion() {
        return this.#content['version'] ? this.#content['version'] : '';
    }

    /**
     * Returns the minecraft version used with this packwiz
     * @returns {string}
     */
    getMcVersion() {
        return this.#content['versions']['minecraft'];
    }

    /**
     * Returns the loader used with this packwiz
     * @returns {string}
     */
    getLoader() {
        return this.#content['versions'] ? Object.keys(this.#content['versions']).filter((elem) => elem !== 'minecraft')[0] : 'unknown';
    }
}

module.exports = Pack;