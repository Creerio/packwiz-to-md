/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const path = require('path');
const toml = require('toml');
const Mod = require('./mod');
const ResourcePack = require('./resourcepack');
const Modrinth = require('./../sources/modrinth');
const CurseForge = require('./../sources/curseforge');
const { isVerbose } = require('../utils');

/**
 * Used to read the index file, finds every mod/resource pack used and extracts them as a md file
 */
class Index {
    static indexFile = 'index.toml';
    #mods = new Map();
    #resourcePacks = new Map();
    #libs = new Map();
    #mrRequest = new Set();
    #cfRequest = new Set();
    #quiltEquivalent = new Map();

    /**
     * Construct the index, finds every mod/resource pack to parse
     * @param filePath Direct path to the index file
     */
    constructor(filePath) {
        // Quilt equivalent, mostly for fabric-api & fabric language kotlin
        // FAPI
        this.#quiltEquivalent.set('P7dR8mSH', 'qvIfYCYJ');
        this.#quiltEquivalent.set('306612', '634179');
        // Fabric language Kotlin
        this.#quiltEquivalent.set('Ha28R6CL', 'lwVhp9o5');
        this.#quiltEquivalent.set('308769', '720410');


        if (!fs.existsSync(filePath)) {
            console.error(filePath + ' not found');
            process.exit(1);
        }

        // Parse toml file first to get its content
        let content = fs.readFileSync(filePath).toString();
        try {
            content = toml.parse(content);
        }
        catch (e) {
            console.error('Failed to parse ' + filePath + ' see the error below :');
            console.error('Parsing error on line ' + e.line + ', column ' + e.column + ': ' + e.message);
            process.exit(1);
        }

        // Using the content, separate each file and store their ids
        const cwd = filePath.replace(Index.indexFile, '');

        // Separate mods & resource packs, also store future requests
        content['files'].forEach((elem) => {
            if (isVerbose())
                console.debug('Pushing ressource ' + path + ' to the correct Map object');
            if (elem['file']?.startsWith('mods/')) {
                const mod = new Mod(path.join(cwd, elem['file']));
                this.#mods.set(mod.id, mod);

                // Add to <source> request list
                switch (mod.source) {
                case 'modrinth':
                    this.#mrRequest.add(mod.id);
                    break;
                case 'curseforge':
                    this.#cfRequest.add(mod.id);
                    break;
                }
            }
            else if (elem['file']?.startsWith('resourcepacks/')) {
                const rpack = new ResourcePack(path.join(cwd, elem['file']));
                this.#resourcePacks.set(rpack.id, rpack);

                // Add to <source> request list
                switch (rpack.source) {
                case 'modrinth':
                    this.#mrRequest.add(rpack.id);
                    break;
                case 'curseforge':
                    this.#cfRequest.add(rpack.id);
                    break;
                }
            }
        });
    }

    /**
     * Initialises the mods/resource packs with data from their source.
     * MUST be called since we want a description or any other data from CF or Modrinth
     * @param loader Loader used with the current packwiz
     * @param withLib If the libraries have to be separated
     * @returns {Promise<void>}
     */
    async init(loader, withLib) {
        // Required libraries will be separated from the mod list
        if (withLib) {
            if (isVerbose())
                console.debug('Getting every mod version ids');
            const mrVersions = [];
            const cfVersions = [];
            Array.from(this.#mrRequest.values()).forEach((mod) => {
                if (this.#mods.has(mod))
                    mrVersions.push(this.#mods.get(mod).version);
            });
            Array.from(this.#cfRequest.values()).forEach((mod) => {
                if (this.#mods.has(mod))
                    cfVersions.push(this.#mods.get(mod).version);
            });

            if (isVerbose())
                console.debug('Contacting APIs to get version data for each mod');
            const toParse = Modrinth.parseVersionData(await Modrinth.getVersionData(mrVersions));
            const cfRes = CurseForge.parseVersionData(await CurseForge.getVersionData(cfVersions));

            cfRes.forEach((val, key) => {
                toParse.set(key, val);
            });

            if (isVerbose())
                console.debug('Parsing received version data');

            const toRemove = new Set();
            toParse.forEach((dependencies, modId) => {
                if (isVerbose())
                    console.debug('Currently parsing modId ' + modId + ' with dependencies ' + dependencies);

                dependencies.forEach((dependencyId) => {
                    if (isVerbose())
                        console.debug('Currently parsing modId ' + modId + ' with dependency ' + dependencyId);
                    // Check if on quilt, if so we must check if we have an equivalent mod in the packwiz to use it instead. If not, we can use the base dependency
                    let toFind = loader === 'quilt' && this.#quiltEquivalent.has(dependencyId) && this.#mods.get(this.#quiltEquivalent.get(dependencyId)) ? this.#quiltEquivalent.get(dependencyId) : dependencyId;
                    if (this.#mods.has(toFind)) {
                        this.#mods.get(toFind).addDependingMod(this.#mods.get(modId));
                        toRemove.add(toFind);
                    }
                });
            });

            // Remove the dependencies from the mod list and have them in their own list
            toRemove.forEach((dependencyId) => {
                const depMod = this.#mods.get(dependencyId);
                this.#libs.set(dependencyId, depMod);
                this.#mods.delete(dependencyId);
            });
        }

        if (this.#mrRequest.size !== 0) {
            if (isVerbose())
                console.debug('Detected modrinth resources, fetching them');
            const mrData = await Modrinth.getModsData(Array.from(this.#mrRequest.values()));

            // Update modrinth's ressource data if the request succeeded
            mrData.forEach((ressource) => {
                if (isVerbose())
                    console.debug('Currently setting data for mr ressource => ' + ressource);
                const id = ressource['id'];
                if (this.#mods.has(id))
                    this.#mods.get(id).setData(ressource);
                else if (this.#libs.has(id))
                    this.#libs.get(id).setData(ressource);
                else if (this.#resourcePacks.has(id))
                    this.#resourcePacks.get(id).setData(ressource);
            });
        }

        if (this.#cfRequest.size !== 0) {
            if (isVerbose())
                console.debug('Detected curseforge resources, fetching them');
            const cfData = await CurseForge.getModsData(Array.from(this.#cfRequest.values()));

            // Update curseforge's ressource data if the request succeeded
            cfData.forEach((ressource) => {
                if (isVerbose())
                    console.debug('Currently setting data for cf ressource => ' + ressource);
                const id = ressource['id'];
                if (this.#mods.has(id))
                    this.#mods.get(id).setData(ressource);
                else if (this.#libs.has(id))
                    this.#libs.get(id).setData(ressource);
                else if (this.#resourcePacks.has(id))
                    this.#resourcePacks.get(id).setData(ressource);
            });
        }
    }

    /**
     * Returns the detected mod in the packwiz folder
     * @returns {Mod[]}
     */
    getMods() {
        return Array.from(this.#mods.values()).sort(function(a, b) { return a.getName().localeCompare(b.getName()) });
    }

    /**
     * Returns the detected libraries in the packwiz folder
     * @returns {Mod[]}
     */
    getLibs() {
        return Array.from(this.#libs.values()).sort(function(a, b) { return a.getName().localeCompare(b.getName()) });
    }

    /**
     * Returns the detected resource packs in the packwiz folder
     * @returns {ResourcePack[]}
     */
    getResourcePacks(){
        return Array.from(this.#resourcePacks.values()).sort(function(a, b) { return a.getName().localeCompare(b.getName()) });
    }

    /**
     * Generates a Markdown text for the mods and resource packs
     * @returns {string} Markdown text
     */
    getMd() {
        if (isVerbose())
            console.debug('Preparing to return markdown');
        let res = '';

        if (this.#mods.size !== 0) {
            if (isVerbose())
                console.debug('Detected mods');
            res += '## Mods\n' +
                '| Name | Description | Links | Optional? |\n' +
                '|:----:|:-----------:|:-----:|:---------:|\n';

            this.getMods().forEach((mod) => {
                res += mod.getMdLine();
            });

            res += '\n\n';
        }

        if (this.#libs.size !== 0) {
            if (isVerbose())
                console.debug('Detected libraries');

            res += '## Libraries\n' +
                '| Name | Target | Links |\n' +
                '|:----:|:------:|:-----:|\n';

            this.getLibs().forEach((mod) => {
                res += mod.getLibMdLine();
            });

            res += '\n\n';
        }

        if (this.#resourcePacks.size !== 0) {
            if (isVerbose())
                console.debug('Detected resource packs');
            res += '## Resource packs\n' +
                '| Name | Description | Links | Optional? |\n' +
                '|:----:|:-----------:|:-----:|:---------:|\n';

            this.getResourcePacks().forEach((pack) => {
                res += pack.getMdLine();
            });

            res += '\n\n';
        }

        return res;
    }
}

module.exports = Index;