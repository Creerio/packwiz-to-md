/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const path = require('path');
const toml = require('toml');
const Local = require('./../sources/local');
const Modrinth = require('./../sources/modrinth');
const CurseForge = require('./../sources/curseforge');
const { firstCharacterUpper, getSourceUrlName, isVerbose } = require('../utils');

/**
 * Class for any resources
 */
class Resource {
    #fileName;
    #content;
    _data;

    /**
     * Constructor of a base resource
     * @param filePath Path to the file
     */
    constructor(filePath) {
        if (!fs.existsSync(filePath)) {
            console.error(filePath + ' not found');
            process.exit(1);
        }

        this.#fileName = path.parse(filePath).name;
        this.#content = '';
        this._data = new Local();

        // Do not try to parse non toml files
        if (!filePath.endsWith('.toml'))
            return;

        // Parse toml file first to get its content
        this.#content = fs.readFileSync(filePath).toString();
        try {
            this.#content = toml.parse(this.#content);
        }
        catch (e) {
            console.error('Failed to parse ' + filePath + ' see the error below :');
            console.error('Parsing error on line ' + e.line + ', column ' + e.column + ': ' + e.message);
            process.exit(1);
        }

        // Save source for later
        if (this.#content)
            this.source = Object.keys(this.#content['update'])[0];
        else
            this.source = 'local';

        // Get some data depending on source
        switch (this.source) {
        case 'modrinth':
            this.id = this.#content['update'][this.source]['mod-id'];
            this.version = this.#content['update'][this.source]['version'];
            break;

        case 'curseforge':
            this.id = this.#content['update'][this.source]['project-id'];
            this.version = this.#content['update'][this.source]['file-id'];
            break;

        default:
            this.id = undefined;
            this.version = undefined;
        }

    }

    /**
     * Sets the data for the source
     * @param data Data from a source
     */
    setData(data) {
        let dt;
        switch (this.source) {
        case 'modrinth':
            dt = new Modrinth(data);
            break;
        case 'curseforge':
            dt = new CurseForge(data);
            break;
        default:
            dt = new Local();
        }
        this._data = dt;
    }

    /**
     * Returns the resource slug for it's source
     * @returns {*}
     */
    getSlug() {
        return this._data.getSlug();
    }

    /**
     * Returns the name of the resource, or it's file name if there is no toml file
     * @returns {*}
     */
    getName() {
        return this.#content ? this.#content['name'] : this.#fileName;
    }

    /**
     * Returns the resource's description without \n characters
     * @returns {*}
     */
    getDescription() {
        return this._data.getDescription().replaceAll('\n', ' ');
    }

    /**
     * Returns on which side this resource is used
     * @returns {*|string}
     */
    getSide() {
        return this.#content ? this.#content['side'] : '';
    }

    /**
     * Returns if this resource is optional
     * @returns {string}
     */
    getIsOptional() {
        return this.#content?.['option']?.['optional'] ? '✔' : '✖';
    }

    /**
     * Returns why this resource is optional, if it is
     * @returns {*|string}
     */
    getWhyIsOptional() {
        return this.#content?.['option']?.['description'] ? this.#content['option']['description'] : '';
    }

    /**
     * Returns the link to the resource page
     * @returns {*}
     */
    getSourceLink() {
        return this._data.getSourceLink();
    }

    /**
     * Returns the link to the source code of the resource, if there is one
     * @returns {*}
     */
    getCodeLink() {
        return this._data.getCodeLink();
    }

    /**
     * Returns the links parsed properly for markdown use
     * @returns {string}
     */
    getMdLinks() {
        let res = '';
        const source = firstCharacterUpper(this.source);
        const sourceCode = firstCharacterUpper(getSourceUrlName(this.getCodeLink()));

        if (source.length !== 0 && sourceCode.length !== 0)
            res += '[' + firstCharacterUpper(this.source) + '](' + this.getSourceLink() + ') - [' + firstCharacterUpper(getSourceUrlName(this.getCodeLink())) + '](' + this.getCodeLink() + ')';
        else if (sourceCode.length !== 0)
            res += '[' + firstCharacterUpper(getSourceUrlName(this.getCodeLink())) + '](' + this.getCodeLink() + ')';
        else if (source.length !== 0)
            res += '[' + firstCharacterUpper(this.source) + '](' + this.getSourceLink() + ')';
        else
            res += '✖';
        return res;
    }

    /**
     * Returns a markdown line of this resource
     * @returns {string}
     */
    getMdLine() {
        if (isVerbose())
            console.debug('Preparing to return markdown line for ' + this.getName());
        return '| ' + this.getName() + ' | ' + this.getDescription() + ' | ' + this.getMdLinks() + ' | ' + this.getIsOptional() + ' |\n';
    }
}

module.exports = Resource;