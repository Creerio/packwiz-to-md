# Packwiz To Markdown

A program made to read all [packwiz](https://github.com/packwiz/packwiz) mods/resource packs from a folder and extract them as a markdown.

This was made for personal use, while [#286](https://github.com/packwiz/packwiz/pull/286) is being worked upon. You are free to use it, and modify it accordingly to the [Licence](LICENSE).

## Installation/Usage

1. Install [NodeJS](https://nodejs.org/) v20.9 or higher on your system.

2. Launch a terminal inside the project and run `npm i`. This will install all required dependencies to run this program.

3. Run `node --env-file=.env index.js`. If you need a list of options, run `node --env-file=.env index.js -h`.

## FAQ

- How can I get data from CurseForge ?

Unless you want a description and links to the source code, you do not need to do this as [packwiz](https://github.com/packwiz/packwiz) already gives the mod/resource pack name and some other information. If you really need to, you must :

- Create an account at https://docs.curseforge.com/, then retrieve an API key.

- Then, create an [.env](.env) file in the same directory as [index.js](index.js) with the key `CF_TOKEN=` and your token, it should look like this :
```
CF_TOKEN=<Your token>
```