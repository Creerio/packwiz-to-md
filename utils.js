/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const tldParser = require('tld-extract');

let useragent;
let verbose = false;

/**
 * Returns the useragent for the requests
 * @returns {*|string}
 */
function getUserAgent() {
    if (useragent)
        return useragent;

    // Security : In case the index file is not called for whatever reason
    const dt = JSON.parse(fs.readFileSync('./package.json').toString());
    useragent = dt.homepage ? dt.homepage.replace(/(^\w+:|^)\/\//, '') + '/' + dt.version : dt.author + '/' + dt.name + '/' + dt.version;

    return useragent;
}

/**
 * Sets the user agent from the provided package.json data
 * @param dt Data from the package.json
 */
function setUserAgent(dt) {
    if (useragent)
        return;

    useragent = dt.homepage ? dt.homepage.replace(/(^\w+:|^)\/\//, '') + '/' + dt.version : dt.author + '/' + dt.name + '/' + dt.version;
}

/**
 * Returns if verbose is enabled
 * @returns {boolean}
 */
function isVerbose() {
    return verbose;
}

/**
 * Enables verbose
 */
function enableVerbose() {
    verbose = true;
}

/**
 * Puts the first character in upper
 * @param string
 * @returns {string}
 */
function firstCharacterUpper(string) {
    if (!string || string.length === 0)
        return '';

    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Returns the source URL to a domain, mainly used for the markdown creation
 * @param string
 * @returns {*|string}
 */
function getSourceUrlName(string) {
    if (!string || string.length === 0 || !string.startsWith('http'))
        return '';

    const domain = tldParser(string);
    return (domain.sub ? domain.sub + '.' : '') + domain.domain.replace(domain.tld, '').slice(0, -1);
}

module.exports = { getUserAgent, setUserAgent, isVerbose, enableVerbose, firstCharacterUpper, getSourceUrlName };