/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Local data, defines default methods which other sources must redefine
 */
class Local {
    /**
     * Returns the slug of the resource
     * @returns {string}
     */
    getSlug() {
        return '';
    }

    /**
     * Returns the description of the resource
     * @returns {string}
     */
    getDescription() {
        return '';
    }

    /**
     * Returns the link to the resource page
     * @returns {string}
     */
    getSourceLink() {
        return '';
    }

    /**
     * Returns the link to the source code of the resource, if there is one
     * @returns {string}
     */
    getCodeLink() {
        return '';
    }
}

module.exports = Local;