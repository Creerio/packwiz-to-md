/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const Local = require('./local');
const { getUserAgent } = require('../utils');

class Curseforge extends Local {
    #data;

    static async getModsData(modIds) {
        if (modIds.length === 0)
            return [];

        if (!process.env.CF_TOKEN) {
            console.warn('No CF_TOKEN environment variable set, skipping getting mod data from CurseForge API');
            return [];
        }

        const body = {
            'filterPcOnly': true,
            'modIds': modIds
        };

        const headers = {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': process.env.CF_TOKEN,
                'User-Agent': getUserAgent(),
            }
        };

        const response = await fetch('https://api.curseforge.com/v1/mods', headers);

        if (response?.ok)
            return (await response.json())['data'];
        else {
            console.error('Failed to recover data from CurseForge\'s API !');
            console.error(response);
            return [];
        }
    }

    static async getVersionData(versionIds) {
        if (versionIds.length === 0)
            return [];

        if (!process.env.CF_TOKEN) {
            console.warn('No CF_TOKEN environment variable set, skipping getting mod data from CurseForge API');
            return [];
        }

        const body = {
            'fileIds': versionIds
        };

        const headers = {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': process.env.CF_TOKEN,
                'User-Agent': getUserAgent(),
            }
        };

        const response = await fetch('https://api.curseforge.com/v1/mods/files', headers);

        if (response?.ok)
            return (await response.json())['data'];
        else {
            console.error('Failed to recover data from CurseForge\'s API !');
            console.error(response);
            return [];
        }
    }

    static parseVersionData(data) {
        if (data.length === 0)
            return data;

        const res = new Map();
        data.forEach((version) => {
            if (version['dependencies']) {
                res.set(version['modId'], []);
                version['dependencies'].forEach((dependency) => {
                    if (dependency['relationType'] && dependency['relationType'] === 3)
                        res.get(version['modId']).push(dependency['modId']);
                });
            }
        });

        return res;
    }

    constructor(data) {
        super();
        this.#data = data;
    }

    getSlug() {
        return this.#data['slug'];
    }

    getDescription() {
        return this.#data['summary'];
    }

    getSourceLink() {
        return 'https://www.curseforge.com/minecraft/mc-mods/' + this.getSlug();
    }

    getCodeLink() {
        return this.#data['links']?.['sourceUrl'] ? this.#data['links']['sourceUrl'] : '' ;
    }
}

module.exports = Curseforge;