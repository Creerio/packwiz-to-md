/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const Local = require('./local');
const { getUserAgent } = require('../utils');

class Modrinth extends Local {
    #data;
    #retry = 0;

    static async getModsData(modIds) {
        if (modIds.length === 0)
            return [];

        const headers = {
            'Accept': 'application/json',
            'User-Agent': getUserAgent()
        };
        const uri = 'https://api.modrinth.com/v2/projects?ids=' + JSON.stringify(modIds);

        return this.modrinthRequest(uri, headers);
    }

    static async getVersionData(versionIds) {
        if (versionIds.length === 0)
            return [];

        const headers = {
            'Accept': 'application/json',
            'User-Agent': getUserAgent()
        };
        const uri = 'https://api.modrinth.com/v2/versions?ids=' + JSON.stringify(versionIds);

        return this.modrinthRequest(uri, headers);
    }

    static async modrinthRequest(uri, headers) {
        const response = await fetch(uri, headers);

        if (response?.ok)
            return await response.json();
        else {
            if (this.#retry < 3) {
                this.#retry++;
                if (response?.headers['X-Ratelimit-Reset']) {
                    console.debug('Ratelimited by Modrinth... Waiting a bit before retrying')
                    await new Promise(resolve => setTimeout(resolve, response.headers['X-Ratelimit-Reset'].value * 1000));
                    return this.modrinthRequest(uri, headers);
                }
            }
            else {
                console.error('Exceeded number of retry with Modrinth.');
                this.#retry = 0;
            }

            console.error('Failed to recover data from Modrinth\'s API !');
            console.error(response);
            return [];
        }
    }

    static parseVersionData(data) {
        if (data.length === 0)
            return data;

        const res = new Map();
        data.forEach((version) => {
            if (version['dependencies']) {
                res.set(version['project_id'], []);
                version['dependencies'].forEach((dependency) => {
                    if (dependency['dependency_type'] && dependency['dependency_type'] === 'required')
                        res.get(version['project_id']).push(dependency['project_id']);
                });
            }
        });

        return res;
    }

    constructor(data) {
        super();
        this.#data = data;
    }

    getSlug() {
        return this.#data['slug'];
    }

    getDescription() {
        return this.#data['description'];
    }

    getSourceLink() {
        return 'https://modrinth.com/mod/' + this.getSlug();
    }

    getCodeLink() {
        return this.#data['source_url'] ? this.#data['source_url'] : '';
    }
}

module.exports = Modrinth;