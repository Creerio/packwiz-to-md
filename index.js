#!/usr/bin/env node
/*
 * PackwizToMd - Markdown generator with packwiz folders
 * Copyright (C) 2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const Pack = require('./files/pack');
const Index = require('./files');
const fs = require('fs');
const { program } = require('commander');
const { enableVerbose, setUserAgent, isVerbose } = require('./utils');

const pkgData = JSON.parse(fs.readFileSync('./package.json').toString());
let inputPath = '.';
let outputPath = 'out.md';

program.option('-f, --force', 'overrides existing output file')
    .option('-i, --input <folder>', 'specifies the packwiz folder to parse', inputPath)
    .option('-l, --libs', 'separates required libraries from the mods list. Be warned that this may be incorrect sometimes')
    .option('-o, --output <file>', 'specifies the markdown file to write to', outputPath)
    .option('-p, --pack', 'adds every information from the pack.toml to the markdown')
    .option('--verbose', 'print debug logs')
    .option('-v, --version', 'print this app\'s version');

program.parse();

const option = program.opts();
if (option.version) {
    console.log(pkgData.version);
    return;
}

if (option.input)
    inputPath = option.input;
if (option.output)
    outputPath = option.output;

// Replace input if using current directory
if (inputPath === '.')
    inputPath = process.cwd();
// Check if given output file exists
if (!option.force && fs.existsSync(outputPath)) {
    console.error('Cannot write to ' + outputPath + ', a file already exists with this name. Please remove it.');
    return;
}
setUserAgent(pkgData);

if (option.verbose)
    enableVerbose();

// Get files from the pack file
if (isVerbose())
    console.debug('Trying to read pack.toml file');
const pack = new Pack(inputPath);

let md = '# ' + pack.getName() + '\n';

if (option.pack) {
    // Not mandatory
    if (pack.getAuthor().length !== 0)
        md += '*By ' + pack.getAuthor() + '*\n\n';

    if (pack.getVersion().length !== 0)
        md += 'v' + pack.getVersion() + '\n\n';

    if (pack.getMcVersion().length !== 0 && pack.getLoader() !== 'unknown')
        md += 'MC ' + pack.getMcVersion() + ' on ' + pack.getLoader() + '\n\n';
    else if (pack.getLoader() === 'unknown')
        md += 'MC ' + pack.getMcVersion() + '\n\n';
    else if (pack.getMcVersion().length === 0)
        md += 'Using ' + pack.getLoader() + ' loader\n\n';
}

const packFiles = pack.getFiles();

// Parse found files
packFiles.forEach((path) => {
    if (isVerbose())
        console.debug('Parsing pack.toml, reading : ' + path);
    if (path.endsWith(Index.indexFile)) {
        if (isVerbose())
            console.debug('index.toml file found, reading it');
        const index = new Index(path);
        index.init(pack.getLoader(), option.libs).then(() => {
            if (isVerbose())
                console.debug('Trying to write markdown file');
            fs.writeFileSync(outputPath, md + index.getMd());
        });
    }
});